/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author MG
 */
public class MGMT_FHE {

    public static int lambda = 512;
    public BigInteger[][][] X;
    public int h = 11;
    BigInteger[] sk;
    static BigInteger p, q, N;

    Random rnd;

    public class MGMT_Cipher {

        public BigInteger[] alphas;

        MGMT_Cipher() {
            alphas = new BigInteger[h];
        }

		public MGMT_Cipher add(MGMT_Cipher a) {
			MGMT_Cipher c = new MGMT_Cipher();
			for (int i = 0; i < h; i++) {
				c.alphas[i] = ((this.alphas[i]).add(a.alphas[i]));
			}
			return c;
		}

		//Sub method
		public MGMT_Cipher sub(MGMT_Cipher a) {
			MGMT_Cipher c = new MGMT_Cipher();
			for (int i = 0; i < h; i++) {
				c.alphas[i] = ((this.alphas[i]).subtract(a.alphas[i])).mod(N);
			}
			return c;
		}

		public MGMT_Cipher mult(BigInteger a) {
            MGMT_Cipher c = new MGMT_Cipher();
            for (int i = 0; i < h; i++) {
                c.alphas[i] = ((this.alphas[i]).multiply(a));
            }
            return c;
        }

        public MGMT_Cipher mult(MGMT_Cipher a) {
            MGMT_Cipher c = new MGMT_Cipher();
            for (int i = 0; i < h; i++) {
                c.alphas[i] = BigInteger.ZERO;
            }
            for (int i = 0; i < h; i++) {
                for (int j = 0; j < h; j++) {
                    BigInteger b = (this.alphas[i]).multiply(a.alphas[j]);
                    for (int k = 0; k < h; k++) {
                        c.alphas[k] = ((c.alphas[k]).add(b.multiply(X[i][j][k]))).mod(N);
                    }

                }
            }
            return c;
        }

        public boolean equals(MGMT_Cipher a, MGMT_Cipher b) {
            return Arrays.equals(a.alphas, b.alphas);
        }

        public String toString() {
            return Arrays.asList(alphas).toString();
        }
    }

    public BigInteger decrypt(MGMT_Cipher C) {
        BigInteger dme = BigInteger.ZERO;
        BigInteger mr;
        for (int i = 0; i < h; i++) {
            dme = (dme.add(C.alphas[i].multiply(sk[i])));
        }
        return dme.mod(p);

    }

    private MGMT_Cipher encrypt_private(BigInteger m) {
        MGMT_Cipher C = new MGMT_Cipher();
        BigInteger Somme = BigInteger.ZERO;
        BigInteger delta = new BigInteger(lambda - 1, rnd);
        BigInteger dme = (delta.multiply(p)).add(m);
        for (int k = 0; k < h - 1; k++) {
            C.alphas[k] = (new BigInteger(2 * lambda, rnd)).mod(N);
            Somme = Somme.add(C.alphas[k].multiply(sk[k]));
        }
        C.alphas[h - 1] = ((dme.subtract(Somme)).mod(N)).multiply(sk[h-1].modInverse(N));
        return C;
    }

    MGMT_FHE(int lambda, int h) {
		this.lambda = lambda;
		this.h = h;
        this.rnd = new Random();
        p = BigInteger.probablePrime(lambda, rnd);
        q = BigInteger.probablePrime(lambda, rnd);;
        N = q.multiply(p);
        sk = new BigInteger[this.h];
        X = new BigInteger[this.h][this.h][this.h];
        for (int i = 0; i < h ; i++) {
            sk[i] = new BigInteger(2 * lambda, rnd).mod(N);
        }
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < h; j++) {
                X[i][j] = encrypt_private(sk[i].multiply(sk[j])).alphas;
            }
        }
    }

	public static BigInteger P(BigInteger x, int degree, BigInteger [] coefs, MGMT_FHE fhe) {
		BigInteger R = BigInteger.ZERO;
		BigInteger xn = BigInteger.ONE;
		for (int i = 0; i <= degree; i++) {	
			R = (R.add(xn.multiply(coefs[i]))).mod(fhe.p);
			xn = (xn.multiply(x)).mod(fhe.p);
		}
		return R;
	}

	public static MGMT_Cipher PH(MGMT_Cipher X, int degree, BigInteger [] coefs, MGMT_FHE fhe) {
		MGMT_Cipher R = fhe.encrypt_private(BigInteger.ZERO);
		MGMT_Cipher xn = fhe.encrypt_private(BigInteger.ONE);
		for (int i = 0; i <= degree; i++) {	
			R = R.add(xn.mult(coefs[i]));
			xn = xn.mult(X);
		}
		return R;
	}

    public static void main(String[] args) {
		int [] lambdas = {256,512,1024,2048};
		int [] degrees = {100,1000,10000};
		long start, end;
		int h = 5;
		for (int lambda : lambdas) {
			for (int degree: degrees) {
				start = System.currentTimeMillis();
				MGMT_FHE fhe = new MGMT_FHE(lambda, h);
				BigInteger [] coefs = new BigInteger[degree + 1];
				for (int i = 0; i <= degree; i++) {
					coefs[i] = new BigInteger(lambda - 1, fhe.rnd);
				}
				BigInteger x,r,r1;
				MGMT_Cipher X, R;
				x = new BigInteger(lambda - 1, fhe.rnd);
				r = P(x, degree, coefs, fhe);
				X = fhe.encrypt_private(x);
				R = PH(X, degree, coefs, fhe);
				r1 = fhe.decrypt(R);
				System.out.println(r.toString());
				System.out.println(r1.toString());
				end = System.currentTimeMillis(); 
				System.out.println("(" + h + "," + lambda + "," + degree + ") = "  + (end - start));
			}
		}
    }
}
